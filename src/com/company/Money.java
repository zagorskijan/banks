package com.company;

public class Money {
    String name;
    double usd;
    double limit;

    public Money(String name, double usd, double limit) {
        this.name = name;
        this.usd = usd;
        this.limit = limit;
    }

    public String getName() {
        return name;
    }

    public double convert(int joe) {
        return joe / usd;
    }
}