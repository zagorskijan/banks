package com.company;

import com.sun.org.apache.xpath.internal.SourceTree;

public class FinanceSystem {

    public void convert(int uah) {
        Money monies[] = new Money[6];

        monies[0] = new Bank("Privat", 26.2);
        monies[1] = new Bank("Unicredit", 26.4);
        monies[2] = new Bank("Some Bank", 26.6);
        monies[3] = new Exchange("Billie's", 27.0);
        monies[4] = new Exchange("James'", 28.0);
        monies[5] = new Exchange("Bobby's place", 26.5);

        for (Money money : monies) {
            System.out.println(money.getName());
            System.out.println(String.format("%.2f", money.convert(uah)));
        }
    }
}