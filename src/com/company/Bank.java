package com.company;

public class Bank extends Money {

    public Bank(String name, double usd) {
        super(name, usd, 150_000);
    }

    @Override
    public double convert(int joe) {
        if (joe > limit) {
            return 0;
        } else {
            return super.convert(joe) * 0.95;
        }
    }
}